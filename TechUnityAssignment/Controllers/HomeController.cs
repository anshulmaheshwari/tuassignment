﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TechUnityAssignment.Models;

namespace TechUnityAssignment.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private static ICollection<Product> products;
        public HomeController()
        {
            if (products == null)
            {
                var random = new Random();
                products = Enumerable.Range(1, 100).Select(x => new Product
                {
                    Discontinued = x % 2 == 1,
                    ProductID = x,
                    ProductName = "Product " + x,
                    UnitPrice = random.Next(1, 1000),

                }).ToList();
            }
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Select([DataSourceRequest]DataSourceRequest request)
        {
           return Json(products.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

      
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, Product product)
        {
            if (product != null && ModelState.IsValid)
            {
                products.Add(product);
            }

            return Json(new[] { product }.ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, Product product)
        {
            if (product != null && ModelState.IsValid)
            {
                Product selectedproduct = products.Where(x => x.ProductID == product.ProductID).FirstOrDefault();
                if(selectedproduct != null)
                {
                    selectedproduct.ProductName = product.ProductName;
                    selectedproduct.Discontinued = product.Discontinued;
                    selectedproduct.UnitPrice = product.UnitPrice;

                }
            }
            return Json(new[] { product }.ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, Product product)
        {
            if (product != null)
            {
                products.Remove(product);
            }

            return Json(new[] { product }.ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }
    }
}