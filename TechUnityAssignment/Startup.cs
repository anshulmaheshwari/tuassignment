﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TechUnityAssignment.Startup))]
namespace TechUnityAssignment
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
